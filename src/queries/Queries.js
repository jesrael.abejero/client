import { gql } from 'apollo-boost'

const getDevicesQuery = gql`
    {
        devices {
            name
            id
            ip
            date
        }
    }
`

const getDeviceQuery = gql`
    query($id: ID){
        device(id: $id){
            id
            name
            ip
            date
            deviceId{
                id
                name
            }
        }
    }
`

export { getDevicesQuery, getDeviceQuery }