import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getDeviceQuery } from '../queries/Queries'

class DeviceDetails extends Component {
    displayDeviceDetails() {
        const { device } = this.props.data
        if (device) {
            return (
                <div>
                    <h4>Device Name: {device.name}</h4>
                    <table className="highlight">
                        <thead>
                            <tr>
                                <th>IP Address</th>
                                <th>Date</th>
                                <th>Device Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{device.ip}</td>
                                <td>{device.date}</td>
                                <td>{device.deviceId.name}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            )
        } else {
            return (
                <div>No Device selected.</div>
            )
        }
    }

    render() {
        return (
            <div id="device-details" className="teal darken-2">
                {this.displayDeviceDetails()}
            </div>
        )
    }
}

export default graphql(getDeviceQuery, {
    options: (props) => {
        return {
            variables: {
                id: props.deviceId
            }
        }
    }
})(DeviceDetails);
