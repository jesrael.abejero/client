import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getDevicesQuery } from '../queries/Queries';
import DeviceDetails from './DeviceDetails';

import { MDBDataTable } from 'mdbreact';

class DeviceList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            selected: null
        }
    }

    handleClick(e, data){
        this.setState({ selected: data })
    }

    displayDevices() {
        let data = this.props.data
        if (data.loading) {
            return (<div>Loading Devices...</div>)
        } else {
            const devices = {
                columns: [
                    {
                        label: 'Name',
                        field: 'name',
                        sort: 'asc',
                        width: 150
                    },
                    {
                        label: 'IP Address',
                        field: 'ip',
                        sort: 'asc',
                        width: 270
                    },
                    {
                        label: 'Date',
                        field: 'date',
                        sort: 'asc',
                        width: 200
                    }
                ],
                rows: data.devices.map(device => {
                    return {
                        name: device.name,
                        ip: device.ip,
                        date: device.date,
                        clickEvent: e => this.handleClick(e, device.id)
                    }
                })
            }
            
            return (
                <MDBDataTable
                    striped
                    bordered
                    responsiveSm
                    responsiveMd
                    responsiveLg
                    entries={5}
                    entriesLabel=""
                    order={['date', 'desc']}
                    data={devices}
                />
            )
        }
    }

    render() {
        return (
            <div>
                <ul id="device-list">
                    {this.displayDevices()}
                </ul>
                <DeviceDetails deviceId={this.state.selected} />
            </div>
        )
    }
}

export default graphql(getDevicesQuery)(DeviceList);
