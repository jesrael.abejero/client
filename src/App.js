import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import Footer from './components/Footer';

// Component
import DeviceList from './components/DeviceList';

// Apollo client setup
const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql'
})

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div id="main" className="container">
          <div id="nav-left" className="teal darken-2">
            <h1>Visitors</h1>
          </div>
          <DeviceList />
        </div>
        <Footer />
      </ApolloProvider>
    );
  }
}

export default App;
